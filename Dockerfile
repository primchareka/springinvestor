FROM steve353/centos:7.4.1708
LABEL Version="1.0.0"
LABEL Description="primchareka"
#making directory we will be working in, calling it app and then giving it executable permissions
RUN mkdir /app && chmod 777 /app

# Download Sun Java
RUN yum -y update
RUN yum -y install wget
RUN yum -y install maven

# Install JConnector and Sun Java 8
RUN wget -nv 'https://www.dropbox.com/s/lqqp8zjc1ibmk8e/jdk-8u131-linux-x64.rpm?dl=0' -O /tmp/jdk-8u131-linux-x64.rpm
RUN yum -y install /tmp/jdk-8u131-linux-x64.rpm

EXPOSE 8082

# Add instructions here to install and start your application ...
COPY target/Investor-0.0.1-SNAPSHOT-exec.jar /app/
WORKDIR /app
ENTRYPOINT ["java", "-jar", "Investor-0.0.1-SNAPSHOT-exec.jar"]

