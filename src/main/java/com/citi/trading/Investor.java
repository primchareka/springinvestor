package com.citi.trading;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Encapsulates a stock investor with a portfolio of stocks, some cash,
 * and the ability to place trade orders.
 * 
 * @author Will Provost
 */
@Component
@Scope("prototype")			// allows spring to create new instances when getBean() is called
public class Investor {

	private Map<String,Integer> portfolio;
	private double cash;
	private int id;
	
	@Autowired
	private OrderPlacer market;

	/**
	 * Handler for trade confirmations.
	 */
	private class NotificationHandler implements Consumer<Trade> {
		public void accept(Trade trade) {
			if (trade.getSize() != 0 && trade.getResult() != Trade.Result.REJECTED) {
				synchronized(Investor.this) {
					String stock = trade.getStock();
					if (trade.isBuy()) {
						if (!portfolio.containsKey(stock)) {
							portfolio.put(stock, 0);
						}
						portfolio.put(stock, portfolio.get(stock) + trade.getSize());
						cash -= trade.getPrice() * trade.getSize();
					} else {
						int shares = portfolio.get(stock) - trade.getSize();
						if (shares != 0) {
							portfolio.put(stock, shares);
						} else {
							portfolio.remove(stock);
						}
						cash += trade.getPrice() * trade.getSize();
					}
				}
			}
		}
	}
	private NotificationHandler handler = new NotificationHandler();
	
	public void setPortfolio(Map<String, Integer> portfolio) {
	
		for (int shares : portfolio.values()) {
			if (shares <= 0) {
				throw new IllegalArgumentException("All share counts must be positive.");
			}
		}
		
		this.portfolio = portfolio;
	}

	public Map<String, Integer> getPortfolio() {
		return portfolio;
	}

	public double getCash() {
		return cash;
	}

	public void setCash(double cash) {
		
		if (cash < 0) {
			throw new IllegalArgumentException("Can't have negative cash.");
		}
		
		this.cash = cash;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		if (id < 0) {
			throw new IllegalArgumentException("Can't have negative id.");
		}
		this.id = id;
	}


	
	/**
	 * Places an order to buy the given stock.
	 */
	public synchronized void buy(String stock, int size, double price) {
		if (size <= 0) {
			throw new IllegalArgumentException("Must buy a positive number of shares.");
		}
		if (size * price > cash) {
			throw new IllegalStateException("Not enough cash to make this purchase.");
		}
		
		Trade trade = new Trade(stock, true, size, price);
		market.placeOrder(trade, handler);
	}
	
	/**
	 * Places an order to sell the given stock.
	 */
	public synchronized void sell(String stock, int size, double price) {
		if (size <= 0) {
			throw new IllegalArgumentException("Must sell a positive number of shares.");
		}
		if (!portfolio.containsKey(stock) || size > portfolio.get(stock)) {
			throw new IllegalStateException("Not enough shares to make this sale.");
		}
		
		Trade trade = new Trade(stock, false, size, price);
		market.placeOrder(trade, handler);
	}

	@Override
	public String toString() {
		return String.format("Investor: [cash=%1.4f, portfolio=%s]", 
				cash, portfolio.toString());
	}
}
