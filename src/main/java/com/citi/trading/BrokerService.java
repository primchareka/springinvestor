package com.citi.trading;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("accounts")
public class BrokerService {

	private Map<Integer, Investor> profile = new HashMap<Integer, Investor>();
	private static int nextId;
	@Autowired
	private ApplicationContext applicationContext;
	
	
	@PostConstruct
	
	public void init() {
		Investor investor1 = applicationContext.getBean(Investor.class);
		nextId=1;
		
		HashMap<String, Integer> portfolio1 = new HashMap<String,Integer>();
		investor1.setId(nextId);
		investor1.setCash(40000);
		investor1.setPortfolio(portfolio1);
		nextId++;
		
		
		Investor investor2 = applicationContext.getBean(Investor.class);
		HashMap<String, Integer> portfolio2 = new HashMap<String,Integer>();
		portfolio2.put("MRK", 100);
		investor2.setId(nextId);
		investor2.setCash(1000);
		investor2.setPortfolio(portfolio2);
		
		nextId++;
		
		profile.put(investor1.getId(), investor1);
		profile.put(investor2.getId(), investor2);
		
	}
	
	public int getNextId() {
		return nextId;
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Investor> getAll() {
		return new ArrayList<Investor>(profile.values());
	}
	
    @RequestMapping(value="/{id}",method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Investor> getByID (@PathVariable("id") int id) {
    	System.out.println("FETCHING ID: " + id);
    	
    	if(profile.containsKey(id)) {
    		
    		Investor investor = profile.get(id);
    		System.out.println("HTTP STATUS: " + HttpStatus.OK);
    		return new ResponseEntity<Investor>(investor, HttpStatus.OK);
    		
    	} else {
    		System.out.println("HTTP STATUS: " + HttpStatus.NOT_FOUND);
    		return new ResponseEntity(HttpStatus.NOT_FOUND);
    		
    	}
	};

    @RequestMapping(method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
	public Investor openAccount(@RequestParam("amt") int amt) {
		if (amt < 0) {
			throw new IllegalArgumentException("Can't have negative cash.");
		}
		
		
		Investor investor = new Investor();
		Map<String,Integer> portfolio = new HashMap<String, Integer>();

		investor.setCash(amt);
		investor.setId(nextId);
		investor.setPortfolio(portfolio);
		
		profile.put(investor.getId(), investor);
		nextId++;
		
		return investor;
	};

	@RequestMapping(path="/delete/{id}" ,method=RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Void> closeAccount(@PathVariable("id") int id) {

		if (!profile.containsKey(id)) {
    		//System.out.println("HTTP STATUS: " + HttpStatus.NOT_FOUND);
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		} else {
			profile.remove(id);
    		//System.out.println("HTTP STATUS: " + HttpStatus.NO_CONTENT);
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}

	};

}
